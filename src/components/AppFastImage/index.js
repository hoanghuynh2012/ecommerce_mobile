import {View, Text} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';

const AppFastImage = ({source, style, resizeMode}) => {
  return <FastImage style={style} source={source} resizeMode={resizeMode} />;
};

export default React.memo(AppFastImage);
