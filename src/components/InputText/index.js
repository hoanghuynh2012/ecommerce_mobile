import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import React from 'react';
import styles from './style';
import AppImageSvg from '../AppImageSvg';
import {AppIcon} from '~assets/icons';
import Colors from '~utils/colors';

const InputText = ({
  label,
  placeholder,
  type,
  value,
  onChangeValue,
  status,
  isPassword,
  toggleSwitch,
  errorText,
}) => {
  return (
    <View
      style={[
        styles.container,
        {
          borderWidth: 0.5,
          borderColor: status === 'failed' ? Colors.Error : Colors.White,
        },
      ]}>
      <View>
        <Text
          style={[
            styles.label,
            {color: status === 'failed' ? Colors.Error : Colors.Gray},
          ]}>
          {label}
        </Text>
      </View>
      <View style={styles.inputView}>
        <TextInput
          secureTextEntry={isPassword}
          style={styles.inputText}
          placeholder={placeholder}
          keyboardType={type}
          value={value}
          onChangeText={val => onChangeValue(val)}
        />
        {status === 'pass' ? (
          <View style={{top: -10}}>
            <AppImageSvg source={AppIcon.iconPass} width={19} height={16} />
          </View>
        ) : status === 'failed' && !toggleSwitch ? (
          <View style={{top: -10}}>
            <AppImageSvg source={AppIcon.iconError} width={14} height={16} />
          </View>
        ) : toggleSwitch ? (
          <TouchableOpacity onPress={() => toggleSwitch()} style={{top: -10}}>
            <AppImageSvg source={AppIcon.iconEye} width={30} height={20} />
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
      {errorText.length > 0 && (
        <View style={styles.errorView}>
          <Text style={styles.errorText}>{errorText}</Text>
        </View>
      )}
    </View>
  );
};

export default React.memo(InputText);
