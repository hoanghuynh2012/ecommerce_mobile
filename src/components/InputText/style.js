import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    width: pxScale.wp(343),
    height: pxScale.hp(64),
    backgroundColor: Colors.White,
    borderRadius: 8,
    marginTop: pxScale.hp(20),
    paddingLeft: pxScale.wp(20),
  },
  label: {
    marginTop: pxScale.hp(14),
    ...Fonts.Regular_11_Gray,
  },
  inputView: {
    flexDirection: 'row',
  },
  inputText: {
    width: '90%',
    height: pxScale.hp(40),
    ...Fonts.Regular_14,
  },
  errorView: {},
  errorText: {
    ...Fonts.Regular_11,
    color: Colors.Error,
  },
});
