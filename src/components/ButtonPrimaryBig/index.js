import {TouchableOpacity, Text} from 'react-native';
import React from 'react';
import styles from './style';

const ButtonPrimaryBig = ({label, style, onClick}) => {
  return (
    <TouchableOpacity onPress={onClick} style={[styles.container, style]}>
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

export default React.memo(ButtonPrimaryBig);
