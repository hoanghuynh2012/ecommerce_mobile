import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import AppImageSvg from '../AppImageSvg';
import {AppIcon} from '~assets/icons';
import styles from './style';

const Header = ({title, buttonLeft, eventButtonLeft}) => {
  return (
    <View style={styles.container}>
      {buttonLeft && (
        <TouchableOpacity onPress={eventButtonLeft} style={styles.buttonRight}>
          <AppImageSvg source={AppIcon.iconGoBack} width={10} height={16} />
        </TouchableOpacity>
      )}
      <View>{title && <Text>{title}</Text>}</View>
      <View></View>
    </View>
  );
};

export default React.memo(Header);
