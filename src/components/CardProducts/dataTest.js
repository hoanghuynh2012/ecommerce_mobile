export default data = [
  {
    id: 1,
    name: 'Evening Dress',
    producers: 'Dorothy Perkins',
    price: 15,
    discount: 20,
    start: 5,
    review: 10,
    status: '',
    image:
      'https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFzaGlvbnxlbnwwfHwwfHw%3D&w=1000&q=80',
  },
  {
    id: 2,
    name: 'T-Shirt Sailing',
    producers: 'Mongo Boy',
    price: 10,
    discount: 0,
    start: 0,
    review: 0,
    status: 'NEW',
    image:
      'https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFzaGlvbnxlbnwwfHwwfHw%3D&w=1000&q=80',
  },
  {
    id: 3,
    name: 'T-Shirt SPANISH    ',
    producers: 'Mango',
    price: 17,
    discount: 0,
    start: 5,
    review: 10,
    status: '',
    image:
      'https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFzaGlvbnxlbnwwfHwwfHw%3D&w=1000&q=80',
  },
  {
    id: 4,
    name: 'T-Shirt',
    producers: 'Dorothy Perkins',
    price: 21,
    discount: 20,
    start: 5,
    review: 10,
    status: '',
    image:
      'https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFzaGlvbnxlbnwwfHwwfHw%3D&w=1000&q=80',
  },
  {
    id: 5,
    name: 'Shirt',
    producers: 'Olivier',
    price: 52,
    discount: 0,
    start: 5,
    review: 10,
    status: 'sold-out',
    image:
      'https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFzaGlvbnxlbnwwfHwwfHw%3D&w=1000&q=80',
  },
];
