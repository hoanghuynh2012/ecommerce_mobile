import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import React from 'react';
import AppImageSvg from '../AppImageSvg';
import {AppIcon} from '~assets/icons';
import styles from './style';
import style from '~screens/HomeScreen/style';
import dataTest from './dataTest';
import ItemProductHome from '../ItemProductHome';

const CardProducts = ({}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.titleHeader}>New</Text>
        <View style={styles.subTitleView}>
          <Text style={styles.subTitleText}>You’ve never seen it before!</Text>
          <TouchableOpacity style={styles.buttonViewAll}>
            <Text style={styles.buttonText}>View All</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.body}>
        {dataTest.map((item, index) => (
          <ItemProductHome data={item} key={index} />
        ))}
      </ScrollView>
    </View>
  );
};

export default React.memo(CardProducts);
