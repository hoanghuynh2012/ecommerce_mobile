import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: pxScale.wp(14),
  },
  header: {
    flexDirection: 'column',
  },
  subTitleView: {
    width: '100%',
    flexDirection: 'row',
    marginTop: pxScale.hp(4),
  },
  titleHeader: {
    ...Fonts.Bold_34,
  },
  subTitleText: {
    ...Fonts.Regular_11_Gray,
  },
  buttonViewAll: {
    marginLeft: pxScale.wp(158),
    top: -pxScale.hp(8),
  },
  buttonText: {
    ...Fonts.Regular_11,
  },
  body: {
    flex: 1,
    marginTop: pxScale.hp(22),
    flexDirection: 'row',
  },
});
