import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import styles from './style';
import AppImageSvg from '../AppImageSvg';

const ButtonLoginWithSocial = ({image}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <AppImageSvg source={image} width={24} height={24} />
    </TouchableOpacity>
  );
};

export default React.memo(ButtonLoginWithSocial);
