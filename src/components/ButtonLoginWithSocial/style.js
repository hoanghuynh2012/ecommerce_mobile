import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    width: pxScale.wp(92),
    height: pxScale.hp(64),
    backgroundColor: Colors.White,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    margin: pxScale.wp(16),
  },
});
