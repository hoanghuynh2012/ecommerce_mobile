import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    width: pxScale.wp(150),
    height: pxScale.hp(260),
    marginRight: pxScale.wp(18),
  },
  viewLeftImage: {
    left: 10,
    top: 10,
    backgroundColor: 'red',
    width: 40,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 29,
  },
  textLeftImage: {
    ...Fonts.SemiBold_10,
    color: Colors.White,
  },
  buttonAddFavorites: {
    left: pxScale.wp(110),
    top: pxScale.hp(140),
    backgroundColor: Colors.White,
    width: pxScale.wp(34),
    height: pxScale.hp(36),
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: Colors.Gray,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  imageView: {
    borderRadius: 10,
  },
  image: {
    width: pxScale.wp(148),
    height: pxScale.hp(184),
  },
  titleProducers: {
    marginTop: pxScale.hp(6),
    ...Fonts.Regular_11_Gray,
  },
  nameProducers: {
    marginTop: pxScale.hp(6),
    ...Fonts.SemiBold_16,
  },
  priceView: {
    flexDirection: 'row',
  },
  price: {
    ...Fonts.Regular_14,
  },
  priceDiscount: {
    marginLeft: pxScale.wp(4),
    ...Fonts.Regular_14,
    color: Colors.Primary,
  },
});
