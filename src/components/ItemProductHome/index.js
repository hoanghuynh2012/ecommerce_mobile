import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import React from 'react';
import styles from './style';
import AppImageSvg from '../AppImageSvg';
import {AppIcon} from '~assets/icons';
import Colors from '~utils/colors';
import AppFastImage from '~components/AppFastImage';
import FastImage from 'react-native-fast-image';
import {discountPrice} from '~utils/func';

const ItemProductHome = ({data}) => {
  return (
    <View style={styles.container}>
      <View style={styles.imageView}>
        <ImageBackground
          style={styles.image}
          imageStyle={{borderRadius: 10}}
          source={{uri: data.image}}>
          {data.discount > 0 ? (
            <View style={styles.viewLeftImage}>
              <Text style={styles.textLeftImage}>{data.discount}</Text>
            </View>
          ) : data.status === 'NEW' ? (
            <View style={styles.viewLeftImage}>
              <Text style={styles.textLeftImage}>{data.status}</Text>
            </View>
          ) : (
            <></>
          )}

          <TouchableOpacity style={styles.buttonAddFavorites}>
            <AppImageSvg source={AppIcon.iconHead} width={13} height={12} />
          </TouchableOpacity>
        </ImageBackground>
      </View>
      <View style={styles.body}>
        <Text style={styles.titleProducers}>{data.producers}</Text>
        <Text style={styles.nameProducers}>{data.name}</Text>
        <View style={styles.priceView}>
          {data.discount > 0 ? (
            <>
              <Text
                style={[
                  styles.price,
                  {
                    textDecorationLine: 'line-through',
                    textDecorationStyle: 'solid',
                    color: Colors.Gray,
                  },
                ]}>
                {data.price}$
              </Text>
              <Text style={styles.priceDiscount}>
                {discountPrice(data.price, data.discount)}$
              </Text>
            </>
          ) : (
            <>
              <Text style={[styles.price]}>{data.price}$</Text>
            </>
          )}
        </View>
      </View>
    </View>
  );
};

export default React.memo(ItemProductHome);
