import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    width: pxScale.wp(343),
    flexDirection: 'row',
    justifyContent: 'space-between',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    borderColor: Colors.Black,
    borderWidth: 2,
    padding: pxScale.hp(20),
  },
  label: {
    ...Fonts.Regular_14,
    color: Colors.Black,
    textTransform: 'uppercase',
  },
});
