import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.Primary,
    width: pxScale.wp(160),
    flexDirection: 'row',
    justifyContent: 'space-between',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    padding: pxScale.hp(20),
  },
  label: {
    ...Fonts.Regular_14,
    color: Colors.White,
    textTransform: 'uppercase',
  },
});
