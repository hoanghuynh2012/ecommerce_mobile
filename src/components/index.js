import Header from './Header';
import AppImageSvg from './AppImageSvg';
import InputText from './InputText';
import ButtonPrimaryBig from './ButtonPrimaryBig';
import ButtonPrimarySmall from './ButtonPrimarySmall';
import ButtonLoginWithSocial from './ButtonLoginWithSocial';
import ButtonOutlineBig from './ButtonOutlineBig';
import CardProducts from './CardProducts';

module.exports = {
  Header,
  AppImageSvg,
  InputText,
  ButtonPrimaryBig,
  ButtonPrimarySmall,
  ButtonLoginWithSocial,
  ButtonOutlineBig,
  CardProducts,
};
