import React from 'react';
import {SvgCss} from 'react-native-svg';

const AppImageSvg = ({source, height, width}) => {
  return <SvgCss width={width} height={height} xml={source} />;
};

export default React.memo(AppImageSvg);
