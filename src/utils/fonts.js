import {pxScale} from '~utils/func';
import Colors from './colors';
const Fonts = {
  Bold_48: {
    fontSize: pxScale.fontSize(48),
    fontFamily: 'Metropolis-Bold',
    fontWeight: '700',
  },
  Bold_34: {
    fontSize: pxScale.fontSize(34),
    fontFamily: 'Metropolis-Bold',
    fontWeight: '700',
  },
  SemiBold_24: {
    fontSize: pxScale.fontSize(24),
    fontFamily: 'Metropolis-SemiBold',
    fontWeight: '600',
  },
  SemiBold_16: {
    fontSize: pxScale.fontSize(16),
    fontFamily: 'Metropolis-SemiBold',
    fontWeight: '600',
  },
  SemiBold_10: {
    fontSize: pxScale.fontSize(11),
    fontFamily: 'Metropolis-SemiBold',
    fontWeight: '600',
  },
  Regular_16: {
    fontSize: pxScale.fontSize(16),
    fontFamily: 'Metropolis-Regular',
    fontWeight: '400',
  },
  Regular_14: {
    fontSize: pxScale.fontSize(16),
    fontFamily: 'Metropolis-Regular',
    fontWeight: '400',
  },
  Regular_11: {
    fontSize: pxScale.fontSize(12),
    fontFamily: 'Metropolis-Regular',
    fontWeight: '400',
  },
  Regular_11_Gray: {
    fontSize: pxScale.fontSize(12),
    fontFamily: 'Metropolis-Regular',
    fontWeight: '400',
    color: Colors.Gray,
  },
};

export default Fonts;
