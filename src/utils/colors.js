const Colors = {
  Black: '#222222',
  Gray: '#9B9B9B',
  Primary: '#DB3022',
  Background: '#E5E5E5',
  White: '#FFFFFF',
  Error: '#F01F0E',
  Success: '#2AA952',
};
export default Colors;
