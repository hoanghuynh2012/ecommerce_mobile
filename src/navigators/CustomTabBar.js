import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import style from '~components/Header/style';
import Colors from '~utils/colors';
import Fonts from '~utils/fonts';
import {AppIcon} from '~assets/icons';
import AppImageSvg from '~components/AppImageSvg';
import {pxScale} from '~utils/func';

const CustomTabBar = ({state, descriptors, navigation}) => {
  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
        let icon;
        switch (label) {
          case 'Home':
            icon = isFocused ? AppIcon.iconHomeActive : AppIcon.iconHome;
            break;
          case 'Shop':
            icon = isFocused ? AppIcon.iconShopActive : AppIcon.iconShop;
            break;
          case 'Bag':
            icon = isFocused ? AppIcon.iconBagActive : AppIcon.iconBag;
            break;
          case 'Favorites':
            icon = isFocused
              ? AppIcon.iconFavoriteActive
              : AppIcon.iconFavorite;
            break;
          case 'Profile':
            icon = isFocused ? AppIcon.iconProfileActive : AppIcon.iconProfile;
            break;
        }
        return (
          <TouchableOpacity
            key={label}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.button}>
            <AppImageSvg source={icon} width={30} height={30} />
            <Text
              style={[
                styles.title,
                {color: isFocused ? Colors.Primary : Colors.Gray},
              ]}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default React.memo(CustomTabBar);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    height: pxScale.hp(83),
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...Fonts.SemiBold_10,
    marginTop: pxScale.hp(2),
  },
});
