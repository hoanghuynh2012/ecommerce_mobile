import React from 'react';
import {Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeScreen} from '~screens';
const Tab = createBottomTabNavigator();
import AppImageSvg from '~components/AppImageSvg';
import {AppIcon} from '~assets/icons';
import Fonts from '~utils/fonts';
import CustomTabBar from './CustomTabBar';

function BottomTabNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <CustomTabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeScreen} icon={AppIcon.iconHome} />
      <Tab.Screen name="Shop" component={HomeScreen} icon={AppIcon.iconHome} />
      <Tab.Screen name="Bag" component={HomeScreen} icon={AppIcon.iconHome} />
      <Tab.Screen
        name="Favorites"
        component={HomeScreen}
        icon={AppIcon.iconHome}
      />
      <Tab.Screen
        name="Profile"
        component={HomeScreen}
        icon={AppIcon.iconHome}
      />
    </Tab.Navigator>
  );
}
export default BottomTabNavigator;
