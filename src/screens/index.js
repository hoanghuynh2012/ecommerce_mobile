import LoginScreen from './LoginScreen';
import SignUpScreen from './SignUpScreen';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import HomeScreen from './HomeScreen';

module.exports = {
  LoginScreen,
  SignUpScreen,
  ForgotPasswordScreen,
  HomeScreen,
};
