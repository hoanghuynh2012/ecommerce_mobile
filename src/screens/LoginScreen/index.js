import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {AppIcon} from '~assets/icons';
import {
  ButtonLoginWithSocial,
  ButtonPrimaryBig,
  Header,
  InputText,
  ButtonOutlineBig,
} from '~components';
import AppImageSvg from '~components/AppImageSvg';

import styles from './style';

const LoginScreen = ({navigation}) => {
  const [state, setState] = React.useState({
    valueEmail: '',
    statusEmail: '',
    errorEmail: '',
    valuePassword: '',
    statusPassword: '',
    errorPassword: '',
    isPassword: true,
  });
  const regexEmail = new RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
  const onChangeValueEmail = val => {
    if (val.length === 0) {
      setState(prevState => ({
        ...prevState,
        statusEmail: '',
      }));
    } else if (regexEmail.test(val)) {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'pass',
        errorEmail: '',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'failed',
        errorEmail: 'Not a valid email address. Should be your@email.com',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valueEmail: val,
    }));
  };

  const onChangeValuePassword = val => {
    if (val.length < 6) {
      setState(prevState => ({
        ...prevState,
        statusPassword: 'failed',
        errorPassword: 'Password length must be between 6 characters',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusPassword: '',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valuePassword: val,
    }));
  };

  const toggleSwitchIsPassword = () => {
    setState(prevState => ({
      ...prevState,
      isPassword: !state.isPassword,
    }));
  };

  const onFinish = () => {
    navigation.replace('BottomTabNavigator');
  };

  const goToSignup = () => {
    navigation.navigate('SignUpScreen');
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={{marginLeft: 14}}>
        <Header />
        <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
          <Text style={styles.textHeader}>Login</Text>
          <View style={styles.form}>
            <InputText
              status={state.statusEmail}
              label="Email"
              errorText={state.errorEmail}
              value={state.valueEmail}
              onChangeValue={onChangeValueEmail}
            />
            <InputText
              status={state.statusPassword}
              label="Password"
              errorText={state.errorPassword}
              isPassword={state.isPassword}
              toggleSwitch={toggleSwitchIsPassword}
              value={state.valuePassword}
              onChangeValue={onChangeValuePassword}
            />
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('ForgotPasswordScreen')}
            style={styles.alreadyView}>
            <Text style={styles.textAlready}>Forgot your password ?</Text>
            <AppImageSvg source={AppIcon.arrowRight} width={19} height={16} />
          </TouchableOpacity>
          <View style={styles.buttonView}>
            <ButtonPrimaryBig label="Login" onClick={onFinish} />
            <ButtonOutlineBig
              label="Create your account"
              style={{marginTop: 20}}
              onClick={goToSignup}
            />
          </View>
          <View style={styles.footerView}>
            <Text style={styles.textFooter}>
              Or sign up with social account
            </Text>
            <View style={styles.buttonFooter}>
              <ButtonLoginWithSocial image={AppIcon.iconGoogle} />
              <ButtonLoginWithSocial image={AppIcon.iconFacebook} />
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;
