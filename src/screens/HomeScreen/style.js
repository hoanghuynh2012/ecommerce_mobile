import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
  bannerView: {
    marginLeft: pxScale.wp(15),
  },
  imageBanner: {
    width: pxScale.wp(417),
    height: pxScale.hp(682),
    justifyContent: 'flex-end',
  },
  textBanner: {
    ...Fonts.Bold_48,
    color: Colors.White,
  },
  body: {
    marginTop: pxScale.hp(33),
  },
});
