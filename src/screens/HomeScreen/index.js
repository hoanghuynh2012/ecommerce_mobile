import {View, Text, ScrollView, ImageBackground} from 'react-native';
import React from 'react';
import styles from './style';
import AppFastImage from '~components/AppFastImage';
import {ButtonPrimarySmall, CardProducts} from '~components';
import images from '~assets/images';
import {pxScale} from '~utils/func';

const HomeScreen = () => {
  return (
    <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
      <ImageBackground
        source={{uri: 'https://wallpapercave.com/wp/wp5111991.jpg'}}
        style={styles.imageBanner}>
        <View style={styles.bannerView}>
          <Text style={styles.textBanner}>Fashion {'\n'} sale</Text>
          <ButtonPrimarySmall
            label={'Check'}
            style={{marginBottom: pxScale.hp(36), marginTop: pxScale.hp(18)}}
          />
        </View>
      </ImageBackground>
      <View style={styles.body}>
        <CardProducts />
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
