import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Background,
  },
  body: {
    marginTop: pxScale.wp(34),
  },
  textHeader: {
    ...Fonts.Bold_34,
    marginTop: pxScale.hp(18),
  },
  form: {
    marginTop: pxScale.hp(87),
  },
  title: {
    ...Fonts.Regular_14,
    width: pxScale.wp(343),
  },
  buttonView: {
    marginTop: pxScale.hp(55),
  },
});
