import {View, Text, ScrollView} from 'react-native';
import React from 'react';
import {
  Header,
  InputText,
  ButtonPrimaryBig,
  ButtonLoginWithSocial,
} from '~components';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './style';
import AppImageSvg from '~components/AppImageSvg';
import {AppIcon} from '~assets/icons';

const ForgotPasswordScreen = ({navigation}) => {
  const [state, setState] = React.useState({
    valueEmail: '',
    statusEmail: '',
    errorEmail: '',
  });
  const regexEmail = new RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
  const onChangeValueEmail = val => {
    if (val.length === 0) {
      setState(prevState => ({
        ...prevState,
        statusEmail: '',
      }));
    } else if (regexEmail.test(val)) {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'pass',
        errorEmail: '',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'failed',
        errorEmail: 'Not a valid email address. Should be your@email.com',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valueEmail: val,
    }));
  };
  const goBack = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView>
      <View style={{marginLeft: 14}}>
        <Header buttonLeft eventButtonLeft={goBack} />
        <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
          <Text style={styles.textHeader}>Forgot password</Text>
          <View style={styles.form}>
            <Text style={styles.title}>
              Please, enter your email address. You will receive a link to
              create a new password via email.
            </Text>
            <InputText
              status={state.statusEmail}
              label="Email"
              errorText={state.errorEmail}
              value={state.valueEmail}
              onChangeValue={onChangeValueEmail}
            />
          </View>
          <View style={styles.buttonView}>
            <ButtonPrimaryBig label="send" />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default ForgotPasswordScreen;
