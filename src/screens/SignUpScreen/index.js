import {View, Text, TouchableOpacity, ScrollView, Keyboard} from 'react-native';
import React from 'react';
import {
  Header,
  InputText,
  ButtonPrimaryBig,
  ButtonLoginWithSocial,
} from '~components';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './style';
import AppImageSvg from '~components/AppImageSvg';
import {AppIcon} from '~assets/icons';

const SignUpScreen = ({navigation}) => {
  const [state, setState] = React.useState({
    valueName: '',
    statusName: '',
    errorName: '',
    valueEmail: '',
    statusEmail: '',
    errorEmail: '',
    valuePassword: '',
    statusPassword: '',
    errorPassword: '',
    valueConfirmPassword: '',
    statusConfirmPassword: '',
    errorConfirmPassword: '',
    isPassword: true,
    isKeyboardVisible: false,
  });

  const regexName = new RegExp(/^[a-zA-Z ]+$/);
  const regexEmail = new RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
  const onChangeValueName = val => {
    if (regexName.test(val)) {
      setState(prevState => ({
        ...prevState,
        statusName: 'pass',
        errorName: '',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusName: 'failed',
        errorName: 'Name are irrelevant.',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valueName: val,
    }));
  };

  const onChangeValueEmail = val => {
    if (val.length === 0) {
      setState(prevState => ({
        ...prevState,
        statusEmail: '',
      }));
    } else if (regexEmail.test(val)) {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'pass',
        errorEmail: '',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusEmail: 'failed',
        errorEmail: 'Not a valid email address. Should be your@email.com',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valueEmail: val,
    }));
  };

  const onChangeValuePassword = val => {
    if (val.length < 6) {
      setState(prevState => ({
        ...prevState,
        statusPassword: 'failed',
        errorPassword: 'Password length must be between 6 characters',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusPassword: '',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valuePassword: val,
    }));
  };

  const onChangeValueConfirmPassword = val => {
    if (val.length < 6) {
      setState(prevState => ({
        ...prevState,
        statusConfirmPassword: 'failed',
        errorConfirmPassword: 'Password length must be between 6 characters',
      }));
    } else if (state.valueConfirmPassword !== state.valuePassword) {
      setState(prevState => ({
        ...prevState,
        statusConfirmPassword: 'failed',
        errorConfirmPassword: 'Password not match',
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        statusConfirmPassword: '',
      }));
    }
    setState(prevState => ({
      ...prevState,
      valueConfirmPassword: val,
    }));
  };
  const toggleSwitchIsPassword = () => {
    setState(prevState => ({
      ...prevState,
      isPassword: !state.isPassword,
    }));
  };

  const goBack = () => {
    navigation.goBack();
  };

  React.useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setState(prevState => ({
          ...prevState,
          isKeyboardVisible: true,
        }));
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setState(prevState => ({
          ...prevState,
          isKeyboardVisible: false,
        }));
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{marginLeft: 14}}>
        <Header buttonLeft eventButtonLeft={goBack} />
        <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
          <Text style={styles.textHeader}>Sign Up</Text>
          <View style={styles.form}>
            <InputText
              status={state.statusName}
              label="Name"
              errorText={state.errorName}
              value={state.valueName}
              onChangeValue={onChangeValueName}
            />
            <InputText
              status={state.statusEmail}
              label="Email"
              errorText={state.errorEmail}
              value={state.valueEmail}
              onChangeValue={onChangeValueEmail}
            />
            <InputText
              status={state.statusPassword}
              label="Password"
              errorText={state.errorPassword}
              isPassword={state.isPassword}
              toggleSwitch={toggleSwitchIsPassword}
              value={state.valuePassword}
              onChangeValue={onChangeValuePassword}
            />
            <InputText
              status={state.statusConfirmPassword}
              label="Confirm Password"
              errorText={state.errorConfirmPassword}
              isPassword={state.isPassword}
              toggleSwitch={toggleSwitchIsPassword}
              value={state.valueConfirmPassword}
              onChangeValue={onChangeValueConfirmPassword}
            />
          </View>
          <TouchableOpacity
            onPress={() => navigation.replace('LoginScreen')}
            style={styles.alreadyView}>
            <Text style={styles.textAlready}>Already have an account?</Text>
            <AppImageSvg source={AppIcon.arrowRight} width={19} height={16} />
          </TouchableOpacity>
          <View style={styles.buttonView}>
            <ButtonPrimaryBig label="Sign Up" />
          </View>
          <View style={styles.footerView}>
            <Text style={styles.textFooter}>
              Or sign up with social account
            </Text>
            <View style={styles.buttonFooter}>
              <ButtonLoginWithSocial image={AppIcon.iconGoogle} />
              <ButtonLoginWithSocial image={AppIcon.iconFacebook} />
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default SignUpScreen;
