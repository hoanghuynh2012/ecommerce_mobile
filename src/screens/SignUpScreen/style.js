import {StyleSheet} from 'react-native';
import Colors from '~utils/colors';
import {pxScale} from '~utils/func';
import Fonts from '~utils/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Background,
  },
  body: {
    marginTop: pxScale.wp(15),
  },
  textHeader: {
    ...Fonts.Bold_34,
    marginTop: pxScale.hp(18),
  },
  form: {
    marginTop: pxScale.hp(60),
  },
  alreadyView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginRight: pxScale.wp(20),
    marginTop: pxScale.hp(20),
  },
  textAlready: {
    ...Fonts.Regular_14,
    marginRight: pxScale.wp(4),
  },
  buttonView: {
    marginTop: pxScale.hp(26),
  },
  footerView: {marginTop: pxScale.hp(60), alignItems: 'center'},
  textFooter: {
    ...Fonts.Regular_14,
  },
  buttonFooter: {
    flexDirection: 'row',
  },
});
